import Vue from "vue";
import members from "@/assets/fixtures/members";

const state = {
  pagination: {
    descending: true,
    currentPage: 1,
    rowsPerPage: 8,
    sortBy: "fat",
    totalItems: 0,
    rowsPerPageItems: [1, 2, 4, 8, 16],
  },
  items: [],
  allItems: [],
  loading: false,
};
const mutations = {
  setPagination(state, payload) {
    for (const property of Object.keys(payload)) {
      Vue.set(state.pagination, property, payload[property]);
    }
  },
  _setLoading(state, payload) {
    state.loading = payload;
  },
  _setItems(state, { items, totalItems }) {
    state.items = items;
    Vue.set(state.pagination, "totalItems", totalItems);
  },
  _setAllItems(state, { allItems }) {
    state.allItems = allItems;
  },
};
const actions = {
  queryItems(context) {
    context.commit("_setLoading", true);
    return new Promise((resolve) => {
      const { sortBy, descending, currentPage, rowsPerPage } =
        context.state.pagination;

      setTimeout(() => {
        let items = members.slice();

        const totalItems = items.length;
        context.commit("_setAllItems", { allItems: items });

        if (sortBy) {
          items = items.sort((a, b) => {
            const sortA = a[sortBy];
            const sortB = b[sortBy];

            if (descending) {
              if (sortA < sortB) return 1;
              if (sortA > sortB) return -1;
              return 0;
            } else {
              if (sortA < sortB) return -1;
              if (sortA > sortB) return 1;
              return 0;
            }
          });
        }

        if (rowsPerPage > 0) {
          items = items.slice(
            (currentPage - 1) * rowsPerPage,
            currentPage * rowsPerPage
          );
        }

        context.commit("_setItems", { items, totalItems });
        context.commit("_setLoading", false);

        resolve();
      }, 500);
    });
  },
  searchItems(context, { query }) {
    return new Promise((resolve) => {
      const { sortBy, descending, currentPage, rowsPerPage } =
        context.state.pagination;

      setTimeout(() => {
        let items = members.slice();

        if (sortBy) {
          items = items.sort((a, b) => {
            const sortA = a[sortBy];
            const sortB = b[sortBy];

            if (descending) {
              if (sortA < sortB) return 1;
              if (sortA > sortB) return -1;
              return 0;
            } else {
              if (sortA < sortB) return -1;
              if (sortA > sortB) return 1;
              return 0;
            }
          });
        }

        if (query) items = items.filter((item) => item.name.includes(query));
        const totalItems = items.length;
        context.commit("_setAllItems", { allItems: items });
        if (rowsPerPage > 0) {
          items = items.slice(
            (currentPage - 1) * rowsPerPage,
            currentPage * rowsPerPage
          );
        }

        context.commit("_setItems", { items, totalItems });

        resolve();
      }, 100);
    });
  },
};
const getters = {
  loading(state) {
    return state.loading;
  },
  pagination(state) {
    return state.pagination;
  },
  items(state) {
    return state.items;
  },
  totalItems(state) {
    return state.pagination.totalItems;
  },
  totalPages(state) {
    return Math.ceil(
      state.pagination.totalItems / state.pagination.rowsPerPage
    );
  },
  currentPage(state) {
    return state.pagination.currentPage;
  },
  rowsPerPageItems(state) {
    return state.pagination.rowsPerPageItems;
  },
  rowsPerPage(state) {
    return state.pagination.rowsPerPage;
  },
  joinedCount(state) {
    return state.allItems.filter((item) => item).length;
  },
  nonjoinedCount(state) {
    return state.allItems.filter((item) => !item).length;
  },
  filledSurveyCount(state) {
    return state.allItems.filter((item) => item.filled_survey).length;
  },
  nonfilledSurveyCount(state) {
    return state.allItems.filter((item) => !item.filled_survey).length;
  },
  acceptedPlanCount(state) {
    return state.allItems.filter((item) => item.accepted_plan).length;
  },
  nonacceptedPlanCount(state) {
    return state.allItems.filter((item) => !item.accepted_plan).length;
  },
  recommendedCount(state) {
    return state.allItems.filter((item) => item.recommended).length;
  },
  nonrecommendedCount(state) {
    return state.allItems.filter((item) => !item.recommended).length;
  },
  loggedTripsCount(state) {
    return state.allItems.reduce((acc, item) => acc + item.logged_trips, 0);
  },
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
